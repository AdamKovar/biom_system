import os
import cv2
import numpy as np
import time

import matplotlib.pyplot as plt

from scipy.spatial import distance
from six.moves import cPickle as pickle

face_priemer = 5708.650087483724
face_rozptyl = 2120961.3047598666
face_threshol = -4



def pca():
    matrix_test = None
    dir_path_list = []
    all_img_list = []
    all_labels = []
    for dir in os.listdir('faces/'):
        extension = os.path.splitext(dir)[1]
        dir_path_list.append(os.path.join('faces/', dir))

    for dirPath in dir_path_list:
        for dir in os.listdir(dirPath):
            all_img_list.append(dirPath + '/' + dir)
            all_labels.append(dirPath.split('/')[1])

    print('all labels')
    print(all_labels)
    print('all img list')
    print(all_img_list)
    print('all listiir')
    print('alll dir_paht list')
    print(dir_path_list)

    for image in all_img_list:
        print(image)
        imgraw = cv2.imread(image, 0)
        imgvector = imgraw.reshape(128 * 128)
        sz = imgraw.shape
        try:
            matrix_test = np.vstack((matrix_test, imgvector))
        except:
            matrix_test = imgvector

    mean, eigenvectors = cv2.PCACompute(matrix_test, np.mean(matrix_test, axis=0).reshape(1, -1))

    vector = cv2.PCAProject(matrix_test, mean, eigenvectors)
    print(vector.shape)

    print('MEAN')
    print(mean)
    print('eigenvectors')
    print(eigenvectors)
    print('sz')
    print(sz)
    print('show image')
    print(all_img_list)

    return all_labels, vector, all_img_list


def createDatasets(matrix, labels):
    train, test = [], []
    train_label, test_label = [], []

    for i, face in enumerate(matrix):
        if labels[i] in test_label:
            train.append(face)
            train_label.append(labels[i])
        else:
            test.append(face)
            test_label.append(labels[i])

    train = np.asarray(train)
    test = np.asarray(test)
    train_label = np.asarray(train_label)
    test_label = np.asarray(test_label)

    train, train_label = shuffle(train, train_label)
    test, test_label = shuffle(test, test_label)


    return train, test, train_label, test_label




def shuffle(dataset, labels):
    permutation = np.random.permutation(len(labels))
    shuffled_dataset = dataset[permutation]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels


def save_pickle(train_dataset, train_labels, test_dataset, test_labels, pickle_file,valid_dataset,valid_label,image_paths):
    try:
        file = open(pickle_file, 'wb')
        newfile = {
            'train_dataset': train_dataset,
            'train_labels': train_labels,
            'test_dataset': test_dataset,
            'test_labels': test_labels,
            'valid_dataset': valid_dataset,
            'valid_labels': valid_label,
            'image_paths': image_paths,
        }
        print('========================================')
        print('Saving pickle file.')
        pickle.dump(newfile, file, pickle.HIGHEST_PROTOCOL)
        file.close()
    except Exception:
        print('Error with save pickle')
    print('========================================')
    return None



def euclidean_distance(people1_data, people2_data, people1_label, people2_label,image_paths, index1, index2):
    compute_distance(people1_data, people2_data, people1_label, people2_label,image_paths, index1, index2)


def compute_distance_all(distance_type, train_dataset, test_dataset, train_label, test_label):
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    ST = 0
    SF = 0
    for i, test in enumerate(test_dataset):
        index = (-1, -1)
        for j, train in enumerate(train_dataset):
            if distance_type == 'manhattan':
                dist = distance.cityblock(train, test)
            elif distance_type == 'euclidean':
                dist = distance.euclidean(train, test)
            index = (i, j)
            z = ((dist - face_priemer) / face_rozptyl) * 10000
            if (z < face_threshol):
                if(test_label[index[0]] == train_label[index[1]]):
                    TP = TP+1
                    ST = ST+1
                else:
                    FP = FP+1

            else:
                if (test_label[index[0]] == train_label[index[1]]):
                    FN = FN + 1

                else:
                    TN = TN + 1
                    SF = SF + 1
    print('TP: ',TP,' FP: ',FP,' FN: ',FN,' TN: ',TN)
    print('System urcil ako zhodne:', ST,'/ 30. System urcil ako rozdielne: ', SF,'/ 270')





def euclidean_distance_all(train_dataset, test_dataset, train_label, test_label):
    compute_distance_all('euclidean', train_dataset, test_dataset, train_label, test_label)


def get_data_from_people(people,people_img, valid_dataset, valid_label):
    poradie = 0
    for i, label in enumerate(valid_label):
        if(label == people):
            poradie = poradie+1
            if(poradie == people_img):
                return valid_dataset[i], valid_label[i], i


def compute_distance(people1_data, people2_data, people1_label, people2_label,image_paths,index1, index2):

    dist = distance.euclidean(people1_data, people2_data)
    z = ((dist - face_priemer) / face_rozptyl)*10000
    if(z < face_threshol):
        print("Je to ono")
        img1 = cv2.imread(image_paths[index1])
        img2 = cv2.imread(image_paths[index2])
        numpy_vertical_concat = np.concatenate((img1, img2), axis=1)
        cv2.imshow('These img are indetical'+image_paths[index1] + ' ' + image_paths[index2], numpy_vertical_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        print("Nie je to ono")
        img1 = cv2.imread(image_paths[index1])
        img2 = cv2.imread(image_paths[index2])
        numpy_vertical_concat = np.concatenate((img1, img2), axis=1)
        cv2.imshow('These img are not indetical'+image_paths[index1]+' '+image_paths[index2], numpy_vertical_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

def rocCurve(train_dataset, test_dataset, train_label, test_label):
    thresholds = [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5, 0, -0.5, -1, -1.5, -2, -2.25, -2.5, -2.75, -3, -3.5,
                  -4, -4.5, -5, -6, -7]
    TPA = []
    FPA = []

    for i, t in enumerate(thresholds):
        TP, FP, FN, TN = euclidean_distance_all_thresh(train_dataset, test_dataset, train_label, test_label, t)
        val1 = TP / 30
        val2 = FP / 270
        TPA.append(val1)
        FPA.append(val2)
    plt.plot(FPA, TPA, 'ro')
    plt.plot(FPA, TPA, 'r')
    plt.axis([0, 1, 0, 1.1])
    plt.grid(True)
    plt.plot([0, 1], [0, 1], '--')
    plt.xlabel('False positive (FP)')
    plt.ylabel('True positive (TP)')
    plt.title('ROC curve (Ear)')
    plt.show()


def compute_distance_all_thresh(distance_type, train_dataset, test_dataset, train_label, test_label, thresh):
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    ST = 0
    SF = 0
    for i, test in enumerate(test_dataset):
        index = (-1, -1)
        for j, train in enumerate(train_dataset):
            if distance_type == 'manhattan':
                dist = distance.cityblock(train, test)
            elif distance_type == 'euclidean':
                dist = distance.euclidean(train, test)
            index = (i, j)
            z = ((dist - face_priemer) / face_rozptyl) * 10000
            if (z < thresh):
                if(test_label[index[0]] == train_label[index[1]]):
                    TP = TP+1
                    ST = ST+1
                else:
                    FP = FP+1

            else:
                if (test_label[index[0]] == train_label[index[1]]):
                    FN = FN + 1

                else:
                    TN = TN + 1
                    SF = SF + 1
    # print('-------------------------------------------------------------')
    # print('Threshold: ',thresh)
    # print('TP: ',TP,' FP: ',FP,' FN: ',FN,' TN: ',TN)
    # print('System urcil ako zhodne:', ST,'/ 30. System urcil ako rozdielne: ', SF,'/ 270')
    # print('Uspesnost: ', ((TN+TP)/300)*100, '%')
    # print('-------------------------------------------------------------')
    return TP, FP, FN, TN



def euclidean_distance_all_thresh(train_dataset, test_dataset, train_label, test_label, thresh):
    return compute_distance_all_thresh('euclidean', train_dataset, test_dataset, train_label, test_label, thresh)




def face_func(people1, people1_img, people2, people2_img):
    valid_label, valid_dataset, all_img_list = pca()
    train_dataset, test_dataset, train_label, test_label = createDatasets(valid_dataset, valid_label)
    save_pickle(train_dataset,train_label,test_dataset,test_label,valid_dataset, valid_label, all_img_list, 'Data/Face_data.pickle')
    euclidean_distance_all(train_dataset, test_dataset, train_label, test_label)
    people1_data, people1_label, index1 = get_data_from_people(people1,people1_img, valid_dataset, valid_label)
    people2_data, people2_label, index2 = get_data_from_people(people2,people2_img, valid_dataset, valid_label)
    euclidean_distance(people1_data, people2_data, people1_label, people2_label, all_img_list, index1, index2)
    rocCurve(train_dataset, test_dataset, train_label, test_label)

    # euclidean_distance_all(train_dataset, test_dataset, train_label, test_label)



