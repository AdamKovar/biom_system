import cv2
import os
import numpy as np
from scipy.spatial import distance
import scipy
import time
import random
from six.moves import cPickle as pickle

from scipy.spatial import distance
from sklearn import svm
from sklearn.neural_network import MLPClassifier

def cropImage(img,file):
    print(img)
    image = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
    img_matrix = scipy.misc.imread(img)
    x_min = 0
    x_max = 250
    y_min = 0
    y_max = 400
    for i in range(399):
        for j in range(249):
            if(img_matrix[i][j] != 0):
                y_min = i
    for i in range(399, 0, -1):
        for j in range(249):
            if(img_matrix[i][j] != 0):
                y_max = i
    for i in range(249):
        for j in range(399):
            if (img_matrix[j][i] != 0):
                x_min = i
    for i in range(249, 0, -1):
        for j in range(399):
            if (img_matrix[j][i] != 0):
                x_max = i
    # result = img_matrix[y_max:y_min][x_max:x_min]
    if( y_min < y_max or x_min < x_max):
        return False
    else:
        crop_img = image[y_max:y_min, x_max:x_min]
        # result = img_matrix[0:250][0:400]
        imgName = 'Data/earCrop2/' + file
        result = cv2.resize(crop_img, (250, 400))
        # cv2.imshow(img, crop_img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        cv2.imwrite(imgName, result)
    return False


def save_pickle(train_dataset, train_labels, test_dataset, test_labels, pickle_file,valid_dataset,valid_label,image_paths):
    try:
        file = open(pickle_file, 'wb')
        newfile = {
            'train_dataset': train_dataset,
            'train_labels': train_labels,
            'test_dataset': test_dataset,
            'test_labels': test_labels,
            'valid_dataset': valid_dataset,
            'valid_labels': valid_label,
            'image_paths': image_paths,
        }
        print('========================================')
        print('Saving pickle file.')
        pickle.dump(newfile, file, pickle.HIGHEST_PROTOCOL)
        file.close()
    except Exception:
        print('Error with save pickle')
    print('========================================')
    return None

def load_pickle(pickle_file):
    print('========================================')
    print('Loading pickle file')
    print('========================================')
    with open(pickle_file, 'rb') as f:
        dataset = pickle.load(f)
        train_data = dataset.get('train_dataset')
        test_data = dataset.get('test_dataset')
        train_labels = dataset.get('train_labels')
        test_labels = dataset.get('test_labels')
        valid_dataset = dataset.get('valid_dataset')
        valid_label = dataset.get('valid_labels')
        image_paths = dataset.get('image_paths')
    return train_data, test_data, train_labels, test_labels, valid_dataset, valid_label, image_paths



def load_images(ears_dir, amount=-1):
    if not os.path.isdir(ears_dir):
        print('Folder %s/ does not exists!!!' % ears_dir)
        return []

    print('Loading %s/ folder...' % ears_dir)
    valid_images = [".bmp", ".png"]
    requested_images = []
    images_name = []
    for i, image_name in enumerate(os.listdir(ears_dir)):
        if amount != -1 and i >= amount:
            break
        ext = os.path.splitext(image_name)[1]
        if ext.lower() not in valid_images:
            continue
        try:
            img_path = os.path.join(ears_dir, image_name)
            image_path_name = img_path.split('/')
            name = image_path_name[2].split('-')[0]
            requested_images.append(img_path)
            images_name.append(name)
        except IOError as e:
            print(e)

    return requested_images, images_name

def pca_transformation(image_paths, processed):
    # Construct the input matrix
    images = [];
    in_matrix = None
    for img_path in image_paths:
        image = cv2.imread(img_path, 0)
        images.append(img_path);
        equalize_img = cv2.equalizeHist(image, 0)
        img_vector = equalize_img.reshape(250 * 400)
        # stack them up to form the matrix
        try:
            in_matrix = np.vstack((in_matrix, img_vector))
        except:
            in_matrix = img_vector

    # PCA
    mean, eigen_vectors = cv2.PCACompute(in_matrix, mean=None)

    vec = cv2.PCAProject(in_matrix, mean, eigen_vectors)
    print('PCA output matrix shape: ' + str(vec.shape))
    return vec

def compute_distance(distance_type, train_dataset, test_dataset, train_label, test_label):
    start = time.clock()
    values = []
    same = []
    sameZ = []
    other = []
    otherZ = []
    result = 0.0
    for i, test in enumerate(test_dataset):
        MIN = 10e100
        index = (-1, -1)
        print('$$$$$$$$$$$$$$$$$')
        print(test_label[i])
        print('$$$$$$$$$$$$$$$$$')
        for j, train in enumerate(train_dataset):
            if distance_type == 'manhattan':
                dist = distance.cityblock(train, test)
            elif distance_type == 'euclidean':
                dist = distance.euclidean(train, test)
            other.append(dist)
            print('Img label: ', train_label[j], ' , Z: ', ((dist - 22157.91) / 17338374.79) * 10000)
            if dist < MIN:
                MIN = dist
                index = (i, j)
            values.append(dist)

        # print('Most similar ear "%s" is "%s"' % (index[0], index[1]), end='\n')


        if test_label[index[0]] == train_label[index[1]]:
            result += 1
            print('Img train label: ', train_label[index[1]], ' test label: ', test_label[index[0]],' , Z: ', ((MIN - 22274) / 17000909) * 10000)
            same.append(MIN)






    val = np.array(values)
    # Variance
    rozptyl = np.var(val)
    # Mean
    priemer = np.mean(val)
    print('%.2f%%  *****************' % ((result / len(test_dataset)) * 100), end=' --> ')
    print('Elapsed time(%f)' % (time.clock() - start))
    print('rozptyl:',rozptyl, ' , priemer: ', priemer)
    for valS in same:
        sameZ.append(((valS - priemer) / rozptyl)*10000)

    #print(same)
    # print(sameZ)
    # print(test_label)
    # print(otherZ)
    # print(train_label)
    for x,i in enumerate(test_label):
        print('Img index: ', i, ' , Z: ', sameZ[x])


def euclidean_distance(train_dataset, test_dataset, train_label, test_label):
    print('*****************  Euclidean distance: ', end='')
    compute_distance('euclidean', train_dataset, test_dataset, train_label, test_label)


def manhattan_distance(train_dataset, test_dataset, train_label, test_label):
    print('*****************  Manhattan distance: ', end='')
    compute_distance('manhattan', train_dataset, test_dataset, train_label, test_label)


def shuffle(dataset, labels):
    permutation = np.random.permutation(len(labels))
    shuffled_dataset = dataset[permutation]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels


def createDatasets(matrix, labels):
    train, test = [], []
    train_label, test_label = [], []

    for i, ear in enumerate(matrix):
        if labels[i] in test_label:
            train.append(ear)
            train_label.append(labels[i])
        else:
            test.append(ear)
            test_label.append(labels[i])

    train = np.asarray(train)
    test = np.asarray(test)
    train_label = np.asarray(train_label)
    test_label = np.asarray(test_label)

    train, train_label = shuffle(train, train_label)
    test, test_label = shuffle(test, test_label)

    return train, test, train_label, test_label

def Task_4(folder):
    files = os.listdir(folder)
    counter = 0
    # image_paths, valid_label = load_images(folder)
    # print('Running PCA...')
    # valid_dataset = pca_transformation(image_paths, folder)
    # train_dataset, test_dataset, train_label, test_label = createDatasets(valid_dataset,valid_label)
    # save_pickle(train_dataset,train_label,test_dataset,test_label,valid_dataset, valid_label, image_paths,'Ear_data.pickle')
    train_dataset, test_dataset, train_label, test_label, valid_dataset, valid_label, image_paths = load_pickle('Ear_data.pickle')
    euclidean_distance(train_dataset, test_dataset, train_label, test_label)
    #manhattan_distance(train_dataset, test_dataset, train_label, test_label)
    # svm(train_dataset, test_dataset, train_label, test_label)
    # mlp(train_dataset, test_dataset, train_label, test_label)
    return True
