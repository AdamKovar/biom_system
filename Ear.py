import cv2
import numpy as np
from six.moves import cPickle as pickle

from scipy.spatial import distance
import matplotlib.pyplot as plt

priemer = 22157.91
rozptyl = 17338374.79
threshold = -2.8

def save_pickle(train_dataset, train_labels, test_dataset, test_labels, pickle_file,valid_dataset,valid_label,image_paths):
    try:
        file = open(pickle_file, 'wb')
        newfile = {
            'train_dataset': train_dataset,
            'train_labels': train_labels,
            'test_dataset': test_dataset,
            'test_labels': test_labels,
            'valid_dataset': valid_dataset,
            'valid_labels': valid_label,
            'image_paths': image_paths,
        }
        print('========================================')
        print('Saving pickle file.')
        pickle.dump(newfile, file, pickle.HIGHEST_PROTOCOL)
        file.close()
    except Exception:
        print('Error with save pickle')
    print('========================================')
    return None

def load_pickle(pickle_file):
    print('========================================')
    print('Loading pickle file')
    print('========================================')
    with open(pickle_file, 'rb') as f:
        dataset = pickle.load(f)
        train_data = dataset.get('train_dataset')
        test_data = dataset.get('test_dataset')
        train_labels = dataset.get('train_labels')
        test_labels = dataset.get('test_labels')
        valid_dataset = dataset.get('valid_dataset')
        valid_label = dataset.get('valid_labels')
        image_paths = dataset.get('image_paths')
    return train_data, test_data, train_labels, test_labels, valid_dataset, valid_label, image_paths





def compute_distance_all(distance_type, train_dataset, test_dataset, train_label, test_label):
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    ST = 0
    SF = 0
    for i, test in enumerate(test_dataset):
        index = (-1, -1)
        for j, train in enumerate(train_dataset):
            if distance_type == 'manhattan':
                dist = distance.cityblock(train, test)
            elif distance_type == 'euclidean':
                dist = distance.euclidean(train, test)
            index = (i, j)
            z = ((dist - priemer) / rozptyl) * 10000
            if (z < threshold):
                if(test_label[index[0]] == train_label[index[1]]):
                    TP = TP+1
                    ST = ST+1
                else:
                    FP = FP+1

            else:
                if (test_label[index[0]] == train_label[index[1]]):
                    FN = FN + 1

                else:
                    TN = TN + 1
                    SF = SF + 1
    print('-------------------------------------------------------------')
    print('Threshold: ',threshold)
    print('---------------------------------------')
    print('TP:\t',TP, ' |\tFP:\t',FP, ' |\tSum:\t',TP+FP,'  |')
    print('---------------------------------------')
    print('TN:\t',TN, '|\tFN:\t',FN, ' |\tSum:\t',FN+TN,' |')
    print('---------------------------------------')
    print('SUM:',TN+TP, '|\tSUM:', FN+FP,'|\tTotal:\t 300  |' )
    print('---------------------------------------')
    print('System identify as the same:', ST,'/',len(train_label),'. System identify as the different: ', SF,'/',(len(test_label)*len(train_label))-len(train_label),'.')
    print('Accuracy for ear identify: ', ((TN+TP)/300)*100, '%')
    print('-------------------------------------------------------------')


def compute_distance_all_thresh(distance_type, train_dataset, test_dataset, train_label, test_label, thresh):
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    ST = 0
    SF = 0
    for i, test in enumerate(test_dataset):
        index = (-1, -1)
        for j, train in enumerate(train_dataset):
            if distance_type == 'manhattan':
                dist = distance.cityblock(train, test)
            elif distance_type == 'euclidean':
                dist = distance.euclidean(train, test)
            index = (i, j)
            z = ((dist - priemer) / rozptyl) * 10000
            if (z < thresh):
                if(test_label[index[0]] == train_label[index[1]]):
                    TP = TP+1
                    ST = ST+1
                else:
                    FP = FP+1

            else:
                if (test_label[index[0]] == train_label[index[1]]):
                    FN = FN + 1

                else:
                    TN = TN + 1
                    SF = SF + 1
    # print('-------------------------------------------------------------')
    # print('Threshold: ',thresh)
    # print('TP: ',TP,' FP: ',FP,' FN: ',FN,' TN: ',TN)
    # print('System urcil ako zhodne:', ST,'/ 30. System urcil ako rozdielne: ', SF,'/ 270')
    # print('Uspesnost: ', ((TN+TP)/300)*100, '%')
    # print('-------------------------------------------------------------')
    return TP, FP, FN, TN




def compute_distance(people1_data, people2_data, people1_label, people2_label,image_paths,index1, index2):

    dist = distance.euclidean(people1_data, people2_data)
    z = ((dist - priemer) / rozptyl)*10000
    if(z < threshold):
        print("Same")
        img1 = cv2.imread(image_paths[index1])
        img2 = cv2.imread(image_paths[index2])
        numpy_vertical_concat = np.concatenate((img1, img2), axis=1)
        cv2.imshow('These img are indetical'+image_paths[index1] + ' ' + image_paths[index2], numpy_vertical_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        print("Different")
        img1 = cv2.imread(image_paths[index1])
        img2 = cv2.imread(image_paths[index2])
        numpy_vertical_concat = np.concatenate((img1, img2), axis=1)
        cv2.imshow('These img are not indetical'+image_paths[index1]+' '+image_paths[index2], numpy_vertical_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return z




def euclidean_distance_all(train_dataset, test_dataset, train_label, test_label):
    compute_distance_all('euclidean', train_dataset, test_dataset, train_label, test_label)


def euclidean_distance_all_thresh(train_dataset, test_dataset, train_label, test_label, thresh):
    return compute_distance_all_thresh('euclidean', train_dataset, test_dataset, train_label, test_label, thresh)


def euclidean_distance(people1_data, people2_data, people1_label, people2_label,image_paths, index1, index2):
    return compute_distance(people1_data, people2_data, people1_label, people2_label,image_paths, index1, index2)


def get_data_from_people(people,people_img, valid_dataset, valid_label):
    poradie = 0
    for i, label in enumerate(valid_label):
        if(label == people):
            poradie = poradie+1
            if(poradie == people_img):
                return valid_dataset[i], valid_label[i], i

def rocCurve(train_dataset, test_dataset, train_label, test_label):
    thresholds = [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5, 0, -0.5, -1, -1.5, -2, -2.25, -2.5, -2.75, -3, -3.5, -4, -4.5, -5, -6, -7]
    TPA = []
    FPA = []

    for i,t in enumerate(thresholds):
        TP, FP, FN, TN = euclidean_distance_all_thresh(train_dataset, test_dataset, train_label, test_label, t)
        val1 = TP/30
        val2 = FP/270
        TPA.append(val1)
        FPA.append(val2)
    plt.plot(FPA, TPA, 'ro')
    plt.plot(FPA, TPA, 'r')
    plt.axis([0, 1, 0, 1.1])
    plt.grid(True)
    plt.plot([0, 1], [0, 1], '--')
    plt.xlabel('False positive (FP)')
    plt.ylabel('True positive (TP)')
    plt.title('ROC curve (Ear)')
    plt.show()

def run(people1, people1_img, people2, people2_img):

    train_dataset, test_dataset, train_label, test_label, valid_dataset, valid_label, image_paths = load_pickle('Data/Ear_data.pickle')
    euclidean_distance_all(train_dataset, test_dataset, train_label, test_label)
    people1_data, people1_label, index1 = get_data_from_people(people1,people1_img, valid_dataset, valid_label)
    people2_data, people2_label, index2 = get_data_from_people(people2,people2_img, valid_dataset, valid_label)
    rocCurve(train_dataset, test_dataset, train_label, test_label)
    return euclidean_distance(people1_data, people2_data, people1_label, people2_label, image_paths, index1, index2)
